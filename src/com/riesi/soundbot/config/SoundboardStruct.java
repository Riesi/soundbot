package com.riesi.soundbot.config;

public class SoundboardStruct {

	private boolean randomSound = false;

	// in s
	private long delayTime = 60;

	// in percent
	private int probability = 100;

	public long getDelayTime() {
		return delayTime;
	}

	public void setDelayTime(long delayTime) {
		this.delayTime = delayTime;
	}

	public int getProbability() {
		return probability;
	}

	public void setProbability(int probability) {
		this.probability = probability;
	}

	public boolean isRandomSound() {
		return randomSound;
	}

	public void setRandomSound(boolean randomSound) {
		this.randomSound = randomSound;
	}

}
