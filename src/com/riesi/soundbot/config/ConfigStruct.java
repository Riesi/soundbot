package com.riesi.soundbot.config;

import java.util.HashMap;
import java.util.Map;

public class ConfigStruct {
	private String token;
	private String ownerId;
	private String prefix = "!";

	private boolean autoReconnect = true;
	private BotMode botmode = BotMode.music;
	private Activity activity = Activity.listening;

	private Map<String, ServerStruct> serverCfgs = new HashMap<>();

	private SoundboardStruct soundboard = new SoundboardStruct();

	public ConfigStruct() {
	}

	/*
	 * Auto generated Getter/Setter
	 */
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public boolean isAutoReconnect() {
		return autoReconnect;
	}

	public void setAutoReconnect(boolean autoReconnect) {
		this.autoReconnect = autoReconnect;
	}

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public BotMode getBotmode() {
		return botmode;
	}

	public void setBotmode(BotMode botmode) {
		this.botmode = botmode;
	}

	public Map<String, ServerStruct> getServerCfgs() {
		return serverCfgs;
	}

	public void setServerCfgs(Map<String, ServerStruct> serverCfgs) {
		this.serverCfgs = serverCfgs;
	}

	public SoundboardStruct getSoundboard() {
		return soundboard;
	}

	public void setSoundboard(SoundboardStruct soundboard) {
		this.soundboard = soundboard;
	}

}
