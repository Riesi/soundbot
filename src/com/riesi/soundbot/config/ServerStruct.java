package com.riesi.soundbot.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class ServerStruct {

	private int volume = 50;

	private long cooldown = 0;

	private boolean autoplaylist = false;

	private boolean blacklisting = false;
	private Vector<String> blacklist = new Vector<>();

	private boolean userdefault = true;
	private Map<String, BotPermission> roles = new HashMap<>();

	public ServerStruct() {
	}

	public int getVolume() {
		return volume;
	}

	public void setVolume(int volume) {
		this.volume = volume;
	}

	public boolean isBlacklisting() {
		return blacklisting;
	}

	public void setBlacklisting(boolean blacklisting) {
		this.blacklisting = blacklisting;
	}

	public Vector<String> getBlacklist() {
		return blacklist;
	}

	public void setBlacklist(Vector<String> blacklist) {
		this.blacklist = blacklist;
	}

	public Map<String, BotPermission> getRoles() {
		return roles;
	}

	public void setRoles(Map<String, BotPermission> roles) {
		this.roles = roles;
	}

	public boolean isUserdefault() {
		return userdefault;
	}

	public void setUserdefault(boolean userdefault) {
		this.userdefault = userdefault;
	}

	public long getCooldown() {
		return cooldown;
	}

	public void setCooldown(long cooldown) {
		this.cooldown = cooldown;
	}

	public boolean isAutoplaylist() {
		return autoplaylist;
	}

	public void setAutoplaylist(boolean autoplaylist) {
		this.autoplaylist = autoplaylist;
	}

}
