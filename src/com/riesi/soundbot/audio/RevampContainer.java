package com.riesi.soundbot.audio;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.util.HaltCatchFire;
import com.riesi.soundbot.util.TrackInfo;
import com.riesi.soundbot.util.Utilities;
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;

public class RevampContainer {

	private static RevampContainer INSTANCE;

	private final RevampAudioManager rAM;
	private static Map<String, RevampAudioPlayer> players = new HashMap<>();

	private RevampContainer() {
		this.rAM = new RevampAudioManager();
		AudioSourceManagers.registerRemoteSources(rAM);
		AudioSourceManagers.registerLocalSource(rAM);// Importat to register for local file play!
	}

	public static RevampContainer GET_INSTANCE() {
		if (INSTANCE == null) {
			INSTANCE = new RevampContainer();
		}
		return INSTANCE;
	}

	public static void RESET() {
		INSTANCE = new RevampContainer();
	}

	public static void setActivity(AudioTrack track) {
		if (players.size() > 1) {
			// on a single server a playing info for soundboards is fine,
			// but on multiple servers it does not make much sense
			if (MainSoundboard.botConfig.getBotmode() == BotMode.soundboard) {
				MainSoundboard.setActivity(null);
				return;
			}
			MainSoundboard.setActivity("Playing on multiple Servers!");
		} else {
			if (track == null) {
				MainSoundboard.setActivity(null);
				return;
			}
			if (track.getInfo().title.equals("Unknown title")) {
				MainSoundboard.setActivity(track.getInfo().uri.replace(MainSoundboard.audioPath.getName(), ""));
			} else {
				MainSoundboard.setActivity(track.getInfo().title);
			}
		}

	}

	/**
	 * Connects the bot to a voice channel and generates a AudioPlayer
	 * 
	 * @param serverId
	 * @param channel
	 * @return The connected AudioPlayer
	 */
	public RevampAudioPlayer connectVoiceChannel(String serverId, VoiceChannel channel) {
		RevampAudioPlayer rev = null;
		synchronized (players) {
			if (!players.containsKey(serverId)) {
				rev = rAM.constructPlayer(serverId);
				players.put(serverId, rev);
			} else {
				rev = players.get(serverId);
			}
			if (rev == null)
				return null;
			rev.setVolume(MainSoundboard.botConfig.getServerCfgs().get(serverId).getVolume());
			rev.connectPlayer(channel);
			loadAutolist(serverId);
		}

		return rev;
	}

	private void loadAutolist(String serverId) {
		if (MainSoundboard.botConfig.getServerCfgs().get(serverId).isAutoplaylist()
				&& MainSoundboard.botConfig.getBotmode() != BotMode.soundboard && players.containsKey(serverId)) {
			players.get(serverId)
					.loadAutoList(Utilities.loadPlaylist(new File(MainSoundboard.plsPath, "playlist_" + serverId + ".txt")));
		}
	}

	public void play(String serverId, VoiceChannel channel, String track, boolean soundboard) {
		RevampAudioPlayer rev = connectVoiceChannel(serverId, channel);
		if (rev != null) {
			rev.playTrack(track, soundboard);
		}
	}

	public void playCall(String serverId, VoiceChannel channel, String track, boolean force,
			CommandCallback commandCallback) {
		RevampAudioPlayer rev = connectVoiceChannel(serverId, channel);
		if (rev != null) {
			rev.addTrackCall(track, commandCallback, force);
		}
	}

	public boolean pause(String serverId, boolean pause) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return false;
		} else {
			rev.setPaused(pause);
		}
		return true;
	}

	public Vector<String[]> getQueue(String serverId) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return null;
		} else {
			return rev.getQueue();
		}
	}

	public boolean flush(String serverId) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return false;
		} else {
			rev.flushQueue();
		}
		return true;
	}

	public boolean stop(String serverId, AudioManager manager) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return false;
		} else {
			rev.stopTrack();
			rev.flushQueue();
			rev.disconnectPlayer(manager);
			players.remove(serverId);
		}
		return true;
	}

	public boolean setVolume(String serverId, int vol) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return true;// since the cfg file is enough
		} else {
			rev.setVolume(vol);
		}
		return true;
	}

	public void playList(String serverId, VoiceChannel voi, List<String> fisherYatesShuffle,
			CommandCallback commandCallback) {
		RevampAudioPlayer rev = connectVoiceChannel(serverId, voi);
		if (rev == null) {
			return;
		} else {
			rev.loadLocalList(new Vector<>(fisherYatesShuffle));
		}
	}

	public boolean skip(String serverId) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return false;
		} else {
			return rev.skipTrack();
		}
	}

	public boolean registerSkip(String serverId, CommandCallback commandCallback) {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return false;
		} else {
			return rev.registerSkip(commandCallback);
		}
	}

	public TrackInfo currentTrack(String serverId) throws HaltCatchFire {
		RevampAudioPlayer rev = players.get(serverId);
		if (rev == null) {
			return null;
		} else {
			return rev.currentTrackInfo();
		}
	}

	public void reloadAutoList(String serverId) {
		loadAutolist(serverId);
	}

	public void playAuto(String serverId, VoiceChannel voi) {
		RevampAudioPlayer rev = connectVoiceChannel(serverId, voi);
		if (rev != null) {
			rev.tryAutoplaylist();
		}
	}
}
