package com.riesi.soundbot.audio;

import static com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason.CLEANUP;
import static com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason.FINISHED;
import static com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason.LOAD_FAILED;
import static com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason.REPLACED;
import static com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason.STOPPED;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.LinkedTransferQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;

import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.util.HaltCatchFire;
import com.riesi.soundbot.util.TrackInfo;
import com.riesi.soundbot.util.Utilities;
import com.sedmelluq.discord.lavaplayer.filter.PcmFilterFactory;
import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.AudioPlayerOptions;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayer;
import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter;
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventListener;
import com.sedmelluq.discord.lavaplayer.player.event.PlayerPauseEvent;
import com.sedmelluq.discord.lavaplayer.player.event.PlayerResumeEvent;
import com.sedmelluq.discord.lavaplayer.player.event.TrackEndEvent;
import com.sedmelluq.discord.lavaplayer.player.event.TrackExceptionEvent;
import com.sedmelluq.discord.lavaplayer.player.event.TrackStartEvent;
import com.sedmelluq.discord.lavaplayer.player.event.TrackStuckEvent;
import com.sedmelluq.discord.lavaplayer.tools.ExceptionTools;
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException;
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist;
import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason;
import com.sedmelluq.discord.lavaplayer.track.InternalAudioTrack;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrame;
import com.sedmelluq.discord.lavaplayer.track.playback.AudioFrameProviderTools;
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame;

/**
 * An audio player that is capable of playing audio tracks and provides audio
 * frames from the currently playing track.
 */
public class RevampAudioPlayer extends DefaultAudioPlayer {

	private Queue<AudioTrack> queue = new LinkedTransferQueue<>();

	private boolean autoOnce = false;
	private boolean idle = true;
	private int autoPointer = 0;
	private List<AudioTrack> autoList = new Vector<>();

	private CommandCallback skCm = null;
	private final String serverId;

	private boolean soundboard = false;

	/**
	 * @param manager Audio player manager which this player is attached to
	 */
	public RevampAudioPlayer(DefaultAudioPlayerManager manager, String serverId) {
		super(manager);
		this.serverId = serverId;
		this.manager = manager;
		activeTrack = null;
		paused = new AtomicBoolean();
		listeners = new ArrayList<>();
		trackSwitchLock = new Object();
		options = new AudioPlayerOptions();
		this.listeners.add(new AudioEventAdapter() {// initilize the adapter here for less annoyance

			@Override
			public void onPlayerPause(AudioPlayer player) {
				// Player was paused
			}

			@Override
			public void onPlayerResume(AudioPlayer player) {
				// Player was resumed
			}

			@Override
			public void onTrackStart(AudioPlayer player, AudioTrack track) {
				synchronized (autoList) {
					if (isAutoplaying() && idle) {
						setStatus(track);
					} else {
						autoOnce = false;
						idle = false;
						System.out.println("Track starting");
						setStatus(track);
					}
					if (skCm != null) {
						skCm.callBackCommand(null);// flush
						skCm = null;
					}
				}
			}

			/*
			 * // endReason == FINISHED: A track finished or died by an exception
			 * (mayStartNext = true). // endReason == LOAD_FAILED: Loading of a track failed
			 * (mayStartNext = true). // endReason == STOPPED: The player was stopped. //
			 * endReason == REPLACED: Another track started playing while this had not
			 * finished // endReason == CLEANUP: Player hasn't been queried for a while, if
			 * you want you can put a // clone of this back to your queue
			 */
			@Override
			public void onTrackEnd(AudioPlayer player, AudioTrack track, AudioTrackEndReason endReason) {
				System.out.println(soundboard + "end?" + endReason.toString() + "?" + endReason.mayStartNext);
				if (endReason.mayStartNext) {
					AudioTrack trackp = null;
					if ((trackp = queue.poll()) == null) {
						idle = true;
						if (!soundboard) {
							if (autoPlay()) {
								return;
							}
						}
					} else {
						autoOnce = false;
						idle = false;
						System.out.println("playing next");
						player.playTrack(trackp);
						setStatus(trackp);
						return;
					}
				}
				soundboard = false;// reset soundboard flag

				switch (endReason) {
				case LOAD_FAILED:
					System.out.println("loading failed");
				case FINISHED:
				case STOPPED:
					System.out.println("finished");
					idle = true;
					break;
				case REPLACED:
					System.out.println("replaced");
					return;// return since the track has been replaced with another one with a valid
									// Status!
				default:
					System.out.println("PaniCCC");
					break;
				}
				setStatus(null);

			}

			@Override
			public void onTrackException(AudioPlayer player, AudioTrack track, FriendlyException exception) {
				// An already playing track threw an exception (track end event will still be
				// received separately)
				System.err.println("Track Exception!");
			}

			@Override
			public void onTrackStuck(AudioPlayer player, AudioTrack track, long thresholdMs) {
				// Audio track has been unable to provide us any audio, might want to just start
				// a new track
				System.err.println("Track is stuck!");
			}
		});
	}

	private boolean autoPlay() {
		if (autoOnce)
			return true;// true since already playing
		autoOnce = true;
		synchronized (autoList) {
			if (isAutoplaying()) {
				if (autoList.isEmpty()) {
					// We don't do anything on an empty list!
					System.err.println("nooo autotracks!");
				} else {
					if (autoPointer < autoList.size()) {
						// play track, but clone it before to prevent wrong state exceptions
						playTrack(autoList.get(autoPointer).makeClone());
						++autoPointer;
					} else {// reshuffle List and reset everyting
						Utilities.fisherYatesShuffle(autoList);
						autoPointer = 1;
						playTrack(autoList.get(0).makeClone());
					}
					return true;
				}
			}
			autoOnce = false;
			return false;
		}
	}

	public void connectPlayer(VoiceChannel channel) {
		AudioManager manager;
		manager = channel.getGuild().getAudioManager();
		manager.setSendingHandler(new AudioPlayerSendHandler(this));
		manager.openAudioConnection(channel);
	}

	public void disconnectPlayer(AudioManager manager) {
		manager.closeAudioConnection();
	}

	public boolean skipTrack() {
		AudioTrack track = null;
		if (((track = queue.poll()) == null)) {
			if (isAutoplaying()) {
				idle = true;
				return autoPlay();
			}
		} else {
			playTrack(track);
			return true;
		}
		return false;
	}

	public boolean registerSkip(CommandCallback commandCallback) {
		if (skCm == null) {
			skCm = commandCallback;
			return true;
		}
		return false;
	}

	public void playTrack(String identifier, boolean soundboard) {
		this.soundboard = soundboard;
		manager.loadItem(identifier, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {
				playTrack(track);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				synchronized (queue) {
					Iterator<AudioTrack> it = playlist.getTracks().iterator();
					if (it.hasNext()) {
						if (idle) {
							autoOnce = false;
							idle = false;
							playTrack(it.next());
							System.out.println("Track playing!");
						} else {
							queue.add(it.next());
							System.out.println("Track queued!");
						}
					}
					while (it.hasNext()) {
						queue.add(it.next());
					}
				}
			}

			@Override
			public void noMatches() {

			}

			@Override
			public void loadFailed(FriendlyException exception) {

			}
		});
	}

	public void addTrackCall(String identifier, CommandCallback call, boolean force) {

		manager.loadItem(identifier, new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {
				if ((force || idle)) {
					idle = false;
					playTrack(track);
					System.out.println("Track playing!");
				} else {
					queue.add(track);
					System.out.println("Track queued!");
				}
				if (call != null)
					call.callBackCommand(track.getInfo().title);
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {
				synchronized (queue) {
					Iterator<AudioTrack> it = playlist.getTracks().iterator();
					if (it.hasNext()) {
						AudioTrack at = it.next();
						if (force || idle) {
							idle = false;
							playTrack(it.next());
							System.out.println("Track playing!");
						} else {
							queue.add(it.next());
							System.out.println("Track queued!");
						}
						if (call != null)
							call.callBackCommand(at.getInfo().title);
					} else {
						call.callBackCommand(null);
					}
					while (it.hasNext()) {
						queue.add(it.next());
					}
				}
			}

			@Override
			public void noMatches() {
				System.out.println("No matches!");
				call.callBackCommand(null);
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				exception.printStackTrace();
				System.out.println("Track loading failed!");
				call.callBackCommand(null);
			}
		});
	}

	public void loadAutoList(Vector<String> autoL) {
		if (autoL == null || autoL.size() == 0)
			return;// mitigate block on empty list

		AudioLoadResultHandler arh = new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {// triggers on valid track
				synchronized (autoList) {
					autoList.add(track);
				}
				autoPlay();
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {// triggers on valid playlist
				synchronized (autoList) {
					for (AudioTrack track : playlist.getTracks()) {
						autoList.add(track);
					}
				}
				autoPlay();
			}

			@Override
			public void noMatches() {
			}

			@Override
			public void loadFailed(FriendlyException exception) {
			}
		};
		for (var t : autoL) {
			manager.loadItem(t, arh);
		}
	}

	public void loadLocalList(Vector<String> locoL) {
		synchronized (queue) {
			queue.clear();
			idle = true;
		}
		AudioLoadResultHandler arh = new AudioLoadResultHandler() {

			@Override
			public void trackLoaded(AudioTrack track) {// triggers on valid track
				synchronized (queue) {
					if (idle) {
						autoOnce = false;
						idle = false;
						playTrack(track);
						System.out.println("Track playing!");
					} else {
						queue.add(track);
						System.out.println("Track queued!");
					}
				}
			}

			@Override
			public void playlistLoaded(AudioPlaylist playlist) {// triggers on valid playlist
				synchronized (queue) {
					Iterator<AudioTrack> it = playlist.getTracks().iterator();
					if (it.hasNext()) {
						if (idle) {
							autoOnce = false;
							idle = false;
							playTrack(it.next());
							System.out.println("Track playing!");
						} else {
							queue.add(it.next());
							System.out.println("Track queued!");
						}
					}
					while (it.hasNext()) {
						queue.add(it.next());
					}
				}
			}

			@Override
			public void noMatches() {// should we remove this track?
				System.err.println("boooooo");
			}

			@Override
			public void loadFailed(FriendlyException exception) {
				System.err.println("nooooo");
			}
		};
		for (var t : locoL) {
			manager.loadItem(t, arh);
		}
	}

	public void flushQueue() {
		synchronized (queue) {
			queue.clear();
		}
	}

	/**
	 * 
	 * @return The queue of the player or an empty Vector
	 */
	public Vector<String[]> getQueue() {
		Vector<String[]> ret = new Vector<>();
		for (var tr : queue) {
			ret.add(new String[] { tr.getInfo().title, tr.getInfo().uri });
		}
		return ret;
	}

	public void setStatus(AudioTrack track) {
		RevampContainer.setActivity(track);
	}

	public boolean isAutoplaying() {
		return MainSoundboard.botConfig.getServerCfgs().get(serverId).isAutoplaylist();
	}

	public TrackInfo currentTrackInfo() throws HaltCatchFire {
		return new TrackInfo(activeTrack, isPaused());
	}

	public void tryAutoplaylist() {
		if (!soundboard) {
//			autoPlay();
		}
	}
	/*
	 * --------------------------------------------------------------
	 */

	private static final Logger log = LoggerFactory.getLogger(AudioPlayer.class);
	private volatile InternalAudioTrack activeTrack;
	private volatile long lastRequestTime;
	private volatile long lastReceiveTime;
	private volatile boolean stuckEventSent;
	private volatile InternalAudioTrack shadowTrack;
	private final AtomicBoolean paused;
	private final DefaultAudioPlayerManager manager;
	private final List<AudioEventListener> listeners;
	private final Object trackSwitchLock;
	private final AudioPlayerOptions options;

	/**
	 * @return Currently playing track
	 */
	public AudioTrack getPlayingTrack() {
		return activeTrack;
	}

	/**
	 * @param track The track to start playing
	 */
	public void playTrack(AudioTrack track) {
		startTrack(track, false);
	}

	/**
	 * @param track       The track to start playing, passing null will stop the
	 *                    current track and return false
	 * @param noInterrupt Whether to only start if nothing else is playing
	 * @return True if the track was started
	 */
	public boolean startTrack(AudioTrack track, boolean noInterrupt) {
		InternalAudioTrack newTrack = (InternalAudioTrack) track;
		InternalAudioTrack previousTrack;

		synchronized (trackSwitchLock) {
			previousTrack = activeTrack;

			if (noInterrupt && previousTrack != null) {
				return false;
			}

			activeTrack = newTrack;
			lastRequestTime = System.currentTimeMillis();
			lastReceiveTime = System.nanoTime();
			stuckEventSent = false;

			if (previousTrack != null) {
				previousTrack.stop();
				dispatchEvent(new TrackEndEvent(this, previousTrack, newTrack == null ? STOPPED : REPLACED));

				shadowTrack = previousTrack;
			}
		}

		if (newTrack == null) {
			shadowTrack = null;
			return false;
		}

		dispatchEvent(new TrackStartEvent(this, newTrack));

		manager.executeTrack(this, newTrack, manager.getConfiguration(), options);
		return true;
	}

	/**
	 * Stop currently playing track.
	 */
	public void stopTrack() {
		stopWithReason(STOPPED);
	}

	private void stopWithReason(AudioTrackEndReason reason) {
		shadowTrack = null;

		synchronized (trackSwitchLock) {
			InternalAudioTrack previousTrack = activeTrack;
			activeTrack = null;

			if (previousTrack != null) {
				previousTrack.stop();
				dispatchEvent(new TrackEndEvent(this, previousTrack, reason));
			}
		}
	}

	private AudioFrame provideShadowFrame() {
		InternalAudioTrack shadow = shadowTrack;
		AudioFrame frame = null;

		if (shadow != null) {
			frame = shadow.provide();

			if (frame != null && frame.isTerminator()) {
				shadowTrack = null;
				frame = null;
			}
		}

		return frame;
	}

	private boolean provideShadowFrame(MutableAudioFrame targetFrame) {
		InternalAudioTrack shadow = shadowTrack;

		if (shadow != null && shadow.provide(targetFrame)) {
			if (targetFrame.isTerminator()) {
				shadowTrack = null;
				return false;
			}

			return true;
		}

		return false;
	}

	@Override
	public AudioFrame provide() {
		return AudioFrameProviderTools.delegateToTimedProvide(this);
	}

	@Override
	public AudioFrame provide(long timeout, TimeUnit unit) throws TimeoutException, InterruptedException {
		InternalAudioTrack track;

		lastRequestTime = System.currentTimeMillis();

		if (timeout == 0 && paused.get()) {
			return null;
		}

		while ((track = activeTrack) != null) {
			AudioFrame frame = timeout > 0 ? track.provide(timeout, unit) : track.provide();

			if (frame != null) {
				lastReceiveTime = System.nanoTime();
				shadowTrack = null;

				if (frame.isTerminator()) {
					handleTerminator(track);
					continue;
				}
			} else if (timeout == 0) {
				checkStuck(track);

				frame = provideShadowFrame();
			}

			return frame;
		}

		return null;
	}

	@Override
	public boolean provide(MutableAudioFrame targetFrame) {
		try {
			return provide(targetFrame, 0, TimeUnit.MILLISECONDS);
		} catch (TimeoutException | InterruptedException e) {
			ExceptionTools.keepInterrupted(e);
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean provide(MutableAudioFrame targetFrame, long timeout, TimeUnit unit)
			throws TimeoutException, InterruptedException {

		InternalAudioTrack track;

		lastRequestTime = System.currentTimeMillis();

		if (timeout == 0 && paused.get()) {
			return false;
		}

		while ((track = activeTrack) != null) {
			if (timeout > 0 ? track.provide(targetFrame, timeout, unit) : track.provide(targetFrame)) {
				lastReceiveTime = System.nanoTime();
				shadowTrack = null;

				if (targetFrame.isTerminator()) {
					handleTerminator(track);
					continue;
				}

				return true;
			} else if (timeout == 0) {
				checkStuck(track);
				return provideShadowFrame(targetFrame);
			} else {
				return false;
			}
		}

		return false;
	}

	private void handleTerminator(InternalAudioTrack track) {
		synchronized (trackSwitchLock) {
			if (activeTrack == track) {
				activeTrack = null;

				dispatchEvent(
						new TrackEndEvent(this, track, track.getActiveExecutor().failedBeforeLoad() ? LOAD_FAILED : FINISHED));
			}
		}
	}

	private void checkStuck(AudioTrack track) {
		if (!stuckEventSent && System.nanoTime() - lastReceiveTime > manager.getTrackStuckThresholdNanos()) {
			stuckEventSent = true;
			dispatchEvent(
					new TrackStuckEvent(this, track, TimeUnit.NANOSECONDS.toMillis(manager.getTrackStuckThresholdNanos()), Thread.currentThread().getStackTrace()));
		}
	}

	public int getVolume() {
		return options.volumeLevel.get();
	}

	public void setVolume(int volume) {
		options.volumeLevel.set(Math.min(1000, Math.max(0, volume)));
	}

	public void setFilterFactory(PcmFilterFactory factory) {
		options.filterFactory.set(factory);
	}

	public void setFrameBufferDuration(Integer duration) {
		if (duration != null) {
			duration = Math.max(200, duration);
		}

		options.frameBufferDuration.set(duration);
	}

	/**
	 * @return Whether the player is paused
	 */
	public boolean isPaused() {
		return paused.get();
	}

	/**
	 * @param value True to pause, false to resume
	 */
	public void setPaused(boolean value) {
		if (paused.compareAndSet(!value, value)) {
			if (value) {
				dispatchEvent(new PlayerPauseEvent(this));
			} else {
				dispatchEvent(new PlayerResumeEvent(this));
				lastReceiveTime = System.nanoTime();
			}
		}
	}

	/**
	 * Destroy the player and stop playing track.
	 */
	public void destroy() {
		stopTrack();
	}

	/**
	 * Add a listener to events from this player.
	 * 
	 * @param listener New listener
	 */
	public void addListener(AudioEventListener listener) {
		synchronized (trackSwitchLock) {
			listeners.add(listener);
		}
	}

	/**
	 * Remove an attached listener using identity comparison.
	 * 
	 * @param listener The listener to remove
	 */
	public void removeListener(AudioEventListener listener) {
		synchronized (trackSwitchLock) {
			for (Iterator<AudioEventListener> iterator = listeners.iterator(); iterator.hasNext();) {
				if (iterator.next() == listener) {
					iterator.remove();
				}
			}
		}
	}

	private void dispatchEvent(AudioEvent event) {
		log.debug("Firing an event with class {}", event.getClass().getSimpleName());

		synchronized (trackSwitchLock) {
			for (AudioEventListener listener : listeners) {
				try {
					listener.onEvent(event);
				} catch (Exception e) {
					log.error("Handler of event {} threw an exception.", event, e);
				}
			}
		}
	}

	@Override
	public void onTrackException(AudioTrack track, FriendlyException exception) {
		dispatchEvent(new TrackExceptionEvent(this, track, exception));
	}

	@Override
	public void onTrackStuck(AudioTrack track, long thresholdMs) {
		dispatchEvent(new TrackStuckEvent(this, track, thresholdMs, Thread.currentThread().getStackTrace()));
	}

	/**
	 * Check if the player should be "cleaned up" - stopped due to nothing using it,
	 * with the given threshold.
	 * 
	 * @param threshold Threshold in milliseconds to use
	 */
	public void checkCleanup(long threshold) {
		AudioTrack track = getPlayingTrack();
		if (track != null && System.currentTimeMillis() - lastRequestTime >= threshold) {
			log.debug("Triggering cleanup on an audio player playing track {}", track);

			stopWithReason(CLEANUP);
		}
	}

}
