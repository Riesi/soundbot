package com.riesi.soundbot;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.commands.configuration.*;
import com.riesi.soundbot.commands.general.Shutdown;
import com.riesi.soundbot.commands.general.*;
import com.riesi.soundbot.commands.moderation.*;
import com.riesi.soundbot.commands.music.*;
import com.riesi.soundbot.commands.soundboard.*;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ConfigStruct;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceDeafenEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.MessageUpdateEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.apache.commons.io.FileUtils;

import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

/*
 * TODO restart still playing current track on reconnect
 * TODO add starts random play
 * TODO info embededMessage?
 */

@SuppressWarnings("unused")
public class MainSoundboard extends ListenerAdapter {

	public static ConfigStruct botConfig = new ConfigStruct();
	public static JDA jda;
	public static Vector<Command> commands = new Vector<>();
	private static Vector<Command> tempCommands = new Vector<>();
	public static boolean restart = false;

	public static File audioPath = new File("audiofiles/");
	public static File plsPath = new File("playlists/");
	private static ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
	private static File cfgFile = new File("config.yaml");

	public MainSoundboard() {

		setCommands();
		orderCommands();
		try {
			JDABuilder builder = JDABuilder.createDefault(botConfig.getToken());
			builder.setStatus(OnlineStatus.DO_NOT_DISTURB);
			jda = builder.build();
					//new JDABuilder(AccountType.BOT).setToken(botConfig.getToken()).setStatus(OnlineStatus.DO_NOT_DISTURB)
			// .buildBlocking(Status.CONNECTED);
			initServerCfgs();// make sure every server has a configuration ... else catch fire!
			jda.getPresence().setStatus(OnlineStatus.IDLE);// bot is to fast not visible...
			jda.setAutoReconnect(botConfig.isAutoReconnect());
		} catch (LoginException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		setActivity(null);
		jda.addEventListener(this);
		jda.getPresence().setStatus(OnlineStatus.ONLINE);
		System.out.println("Ready!");
		System.out.println("You can invite the bot to a server with the following link: \n"
				+ jda.getInviteUrl(Utilities.serverPerms));
	}

	public static void main(String[] args) throws LoginException, RateLimitedException, InterruptedException {
		// create directory for audio files, if it does not exist!
		if (!audioPath.isDirectory())
			audioPath.mkdirs();
		if (!plsPath.isDirectory())
			plsPath.mkdirs();
		if (!cfgFile.exists()) {
			System.err.println(
					"No config file found!\nPlease enter the correct token, owner-id and prefix into the generated file.");
			try {
				storeConfig();
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.err.println("To create a bot use this link:");
			System.err.println("https://discordapp.com/developers/applications/");
			System.exit(-1);
		}

		do {
			restart = false;
			new Thread(new Runnable() {

				@Override
				public void run() {

					try {// load config file
						loadConfig();
					} catch (JsonParseException | JsonMappingException e) {
						System.err.println("Reading the config failed!");
						try {
							FileUtils.copyFile(cfgFile, new File("config_old.yaml"));
							FileUtils.forceDelete(cfgFile);
							storeConfig();
						} catch (JsonGenerationException e1) {
							e1.printStackTrace();
						} catch (JsonMappingException e1) {
							e1.printStackTrace();
						} catch (IOException e1) {
							e1.printStackTrace();
						} finally {
							System.exit(-1);
						}

					} catch (IOException e) {
						e.printStackTrace();
						System.err.println("Loading config failed horribly!");
						System.exit(-1);
					}

					new MainSoundboard();
				}
			}).start();

			while (true) {// hacky stuff for restarting the bot!
				if (jda != null) {
					if (jda.getStatus() != null) {
						if (jda.getStatus() == JDA.Status.SHUTDOWN) {
							jda = null;
							break;
						}
					}
				}
				Thread.sleep(500);
			}
			System.out.println("MainThread is released!");
			RevampContainer.RESET();
		} while (restart);
	}

	/*
	 * Generates a server config for fresh joined server!
	 */
	@Override
	public void onGuildJoin(GuildJoinEvent event) {
		System.out.println("New in the hood!");
		if (!botConfig.getServerCfgs().containsKey(event.getGuild().getId())) {
			botConfig.getServerCfgs().put(event.getGuild().getId(), new ServerStruct());
			try {
				storeConfig();
			} catch (IOException e) {
				System.err.println("Well shit...");
				e.printStackTrace();
			}
		}
	}

	/*
	 * Handling of users joined/leaved/moved/defend All this does is just pausing
	 * and unpausing the specific player of the corresponding server. That's a
	 * pretty neat feature!
	 */
	@Override
	public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {// user joins
		pauseUnpause(event.getGuild());
	}

	@Override
	public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {// user leaves
		pauseUnpause(event.getGuild());
	}

	@Override
	public void onGuildVoiceMove(GuildVoiceMoveEvent event) {
		pauseUnpause(event.getGuild());
	}

	@Override
	public void onGuildVoiceDeafen(GuildVoiceDeafenEvent event) {
		pauseUnpause(event.getGuild());
	}

	/*
	 * Handling of text messages sent and edited ones
	 */
	@Override
	public void onMessageUpdate(MessageUpdateEvent event) {// yes this detects edits
		messageEventHandle(event.getMessage());
	}

	@Override
	public void onMessageReceived(MessageReceivedEvent event) {
		messageEventHandle(event.getMessage());
	}

	private void pauseUnpause(Guild guild) {
		if (Utilities.checkUsersInVoiceChannel(Utilities.findUserInVoice(guild, jda.getSelfUser()))) {
			RevampContainer.GET_INSTANCE().pause(guild.getId(), false);
		} else {
			RevampContainer.GET_INSTANCE().pause(guild.getId(), true);
		}
	}

	private void messageEventHandle(Message message) {
		if (message.getAuthor().isBot())
			return;// escape bot hell

		if (message.isFromType(ChannelType.PRIVATE)) {
			System.out.printf("[PM] %s: %s\n", message.getAuthor().getName(), message.getContentDisplay());
			parseMessage(message, true);

		} else {
			System.out.printf("[%s][%s] %s: %s\n", message.getGuild().getName(), message.getTextChannel().getName(),
					message.getMember().getEffectiveName(), message.getContentDisplay());
			parseMessage(message, false);
		}
	}

	private boolean parseMessage(Message message, boolean privateChat) {
		if (message.getContentRaw().startsWith(botConfig.getPrefix())) {
			String[] arg = message.getContentRaw().split("[" + botConfig.getPrefix() + "]");

			if (arg.length > 0 && arg[1] != null) {
				String[] cmd = arg[1].split(" ");
				if (cmd[0] != "") {
					for (var cmds : commands) {
						if (cmds.getName().equals(cmd[0])) {// executing given command
							if (cmds.getArgumentCnt() > cmd.length) {// argument number check
								message.getChannel().sendMessage("Invalid **number** of arguments!").submit();
								message.getChannel()
										.sendMessage(
												"`" + botConfig.getPrefix() + cmds.getName() + cmds.getArguments() + "` " + cmds.getHelp())
										.submit();
								return false;
							}
							if (!cmds.isPrivateUse() && privateChat) {// private chat check
								message.getChannel().sendMessage("Command **can't** be used in private chat!").submit();
								return false;
							}

							BotPermission perm;
							ServerStruct serverCfg;
							if (privateChat) {
								serverCfg = getHighestPermission(message.getAuthor().getId());
							} else {
								serverCfg = botConfig.getServerCfgs().get(message.getGuild().getId());
							}
							if (!(botConfig.getOwnerId().equals(message.getAuthor().getId()))) {

								// Every server should have a config and the bot should have atleast one server
								// configured for
								// normal users. This is for failure measures.
								if (serverCfg == null) {
									System.err.println("No server config found!");
									serverCfg = new ServerStruct();
								}
								if (serverCfg.getBlacklist().contains(message.getAuthor().getId())) {
									message.getChannel()
											.sendMessage("Sorry " + message.getAuthor().getAsMention() + " are **blacklisted**!").submit();
									return false;
								}

								perm = serverCfg.getRoles().get(message.getAuthor().getId());

								for(var r:message.getAuthor().getJDA().getRoles()){
									if(perm == null){
										perm = serverCfg.getRoles().get(r.getId());
									}

									BotPermission rPerm = serverCfg.getRoles().get(r.getId());
									if(rPerm != null && rPerm.getPermissionValue() > perm.getPermissionValue()){
										perm = rPerm;
									}
								}

								if (perm == null && serverCfg.isUserdefault()) {
									perm = BotPermission.user;
								}
								if (perm == null || cmds.getPermission().getPermissionValue() > perm.getPermissionValue()) {
									message.getChannel().sendMessage("Sorry you **don't** have permissions for that!").submit();
									return false;
								}
							} else {
								perm = BotPermission.owner;// sensei is here!
							}
							System.out.println("Executing: " + cmd[0]);
							if (cmds.executeCommand(message, cmd, perm, serverCfg) && cmds.modifiesCfg()) {// execute command
								try {// store most recent config!
									storeConfig();
								} catch (JsonGenerationException | JsonMappingException e) {
									e.printStackTrace();
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	private void setCommands() {
		tempCommands.clear();
		// configuration
		tempCommands.add(new AutoPlaylist());
		tempCommands.add(new AutoReconnect());
		tempCommands.add(new ChangeMode());
		tempCommands.add(new ChangeName());
		tempCommands.add(new ChangePicture());
		tempCommands.add(new SetActivity());
		tempCommands.add(new SetPrefix());
		tempCommands.add(new SetCooldown());
		tempCommands.add(new SetVolume());
		// general
		tempCommands.add(new ClearMessages());
		tempCommands.add(new Disconnect());
		tempCommands.add(new Help());
		tempCommands.add(new Info());
		tempCommands.add(new Invite());
		tempCommands.add(new Restart());
		tempCommands.add(new Shutdown());
		tempCommands.add(new Summon());
		tempCommands.add(new WhoAmI());
		// moderation
		tempCommands.add(new Blacklist());
		tempCommands.add(new Demote());
		tempCommands.add(new MakeAdmin());
		tempCommands.add(new MakeModerator());
		tempCommands.add(new MakeUser());
		tempCommands.add(new Pardon());
		// music
		tempCommands.add(new Add());
		tempCommands.add(new Delete());
		tempCommands.add(new Flush());
		tempCommands.add(new ForcePlay());
		tempCommands.add(new NowPlaying());
		tempCommands.add(new Pause());
		tempCommands.add(new Play());
		tempCommands.add(new Playlists());
		tempCommands.add(new Queue());
		tempCommands.add(new Resume());
		tempCommands.add(new Skip());
		tempCommands.add(new Stop());
		// soundboard
		tempCommands.add(new AddSound());
		tempCommands.add(new RemoveSound());
		tempCommands.add(new SoundBoard());
		tempCommands.add(new SoundList());
		tempCommands.add(new SetRandomSound());
		tempCommands.add(new SetDelayTime());
		tempCommands.add(new SetProbability());
	}

	public static void orderCommands() {
		commands.clear();// clear commands, because they can be reloaded by changeMode!
		if ((botConfig.getBotmode() == BotMode.debug)) {
			for (var cmds : tempCommands) {// feed everything via loop because setting references would be dumb with clear
				commands.add(cmds);
			}
			return;
		}
		for (var cmds : tempCommands) {
			if ((cmds.getMode() == BotMode.always) || (botConfig.getBotmode() == cmds.getMode())) {
				commands.add(cmds);
				continue;
			}
		}
	}

	/**
	 * Initializes the configurations for each server and saves the config if
	 * modified
	 */
	private void initServerCfgs() {
		boolean save = false;
		for (var guild : jda.getGuilds()) {
			if (!botConfig.getServerCfgs().containsKey(guild.getId())) {
				botConfig.getServerCfgs().put(guild.getId(), new ServerStruct());
				save = true;
			}
		}
		if (save) {
			try {
				storeConfig();
			} catch (IOException e) {
				System.err.println("Well shit...");
				e.printStackTrace();
			}
		}

	}

	/**
	 * @return Returns the users server with the highest permission or the first
	 *         server
	 */
	private ServerStruct getHighestPermission(String id) {
		BotPermission ret = null;
		ServerStruct svr = null;
		for (var svrCfg : botConfig.getServerCfgs().values()) {
			if (ret == null || ret.getPermissionValue() < svrCfg.getRoles().get(id).getPermissionValue()) {
				ret = svrCfg.getRoles().get(id);
				svr = svrCfg;
			}
		}
		return svr;
	}

	private static void loadConfig() throws JsonParseException, JsonMappingException, IOException {
		if (cfgFile.exists()) {
			botConfig = mapper.readValue(cfgFile, ConfigStruct.class);
			// System.out.println(ReflectionToStringBuilder.toString(botConfig,ToStringStyle.MULTI_LINE_STYLE));
		} else {
			storeConfig();
		}
		// initialize null lists to prevent exceptions!
		if (botConfig.getServerCfgs() == null)
			botConfig.setServerCfgs(new HashMap<>());
	}

	private static void storeConfig() throws JsonGenerationException, JsonMappingException, IOException {
		FileOutputStream cfgFos = new FileOutputStream(cfgFile);
		System.out.println("Generating cfg file!");
		mapper.writeValue(cfgFos, botConfig);
		cfgFos.close();
	}

	public static void setActivity(String activity) {
		if (activity == null)
			activity = botConfig.getPrefix() + "help";// default call

		switch (botConfig.getActivity()) {
		case listening:
			jda.getPresence().setActivity(Activity.listening(activity));
			break;
		case watching:
			jda.getPresence().setActivity(Activity.watching(activity));
			break;
		case playing:
			jda.getPresence().setActivity(Activity.playing(activity));
			break;
		case streaming:
			jda.getPresence().setActivity(Activity.playing("Streaming not supported!"));
			break;
		default:
			jda.getPresence().setActivity(Activity.listening(activity));
			break;
		}
	}

	public static void shutdownBot() {
		try {// store most recent config!
			storeConfig();
		} catch (JsonGenerationException | JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Shutting bot down!");
		jda.shutdown();
	}

	public static void exitBot() {
		shutdownBot();
		System.exit(0);
	}
}
