package com.riesi.soundbot.util;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;


public class TrackInfo {

	private AudioTrack at;

	private boolean paused;
	
	/**
	 * 
	 * @param at
	 * @param paused
	 * @throws NullPointerException throws Exception when AudioTrack is null
	 */
	public TrackInfo(AudioTrack at, boolean paused) throws HaltCatchFire{
		if(at==null)
			throw new HaltCatchFire();
		this.at=at;
		this.paused=paused;
	}

	public AudioTrack getAudioTrack() {
		return at;
	}

	public long getDuration() {
		return at.getDuration();
	}

	public long getPosition() {
		return at.getPosition();
	}
	public boolean isPaused() {
		return paused;
	}
	
	
}
