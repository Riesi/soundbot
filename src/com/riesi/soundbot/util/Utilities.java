package com.riesi.soundbot.util;

import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.List;
import java.util.Vector;

public interface Utilities {

	public static final int characterLimit = 1990;// actually 2000, but we leave some space for error :)
	public static final long cooldownLimit = 300;// 5min
	public static final long minimumDelay = 10;

	public static final long nowPlayingBars = 24;// 24 looks good

	public static final Permission[] serverPerms = new Permission[] { Permission.NICKNAME_CHANGE,
			Permission.MESSAGE_WRITE, Permission.MESSAGE_ADD_REACTION, Permission.MESSAGE_EMBED_LINKS,
			Permission.MESSAGE_ATTACH_FILES, Permission.MESSAGE_HISTORY, Permission.VOICE_SPEAK, Permission.VOICE_CONNECT };

	/*
	 * All extensions that should be useful
	 */
	public static final String[] audioExtensions = new String[] { "wav", "mp3", "pls", "flac", "aac", "ogg", "mp4", "m4a",
			"webm", "m3u" };
	public static final String[] imageExtensions = new String[] { "png", "jpg", "gif", "jpeg" };

	/**
	 * 
	 * @param guild  where to look for the author
	 * @param author is the user we are looking for
	 * @return the VoiceChannel of the author or null, if he isn't in one
	 */
	public static VoiceChannel findUserInVoice(Guild guild, User author) {
		if (guild != null && guild.getVoiceChannels() != null) {
			for (var voi : guild.getVoiceChannels()) {
				for (var mem : voi.getMembers()) {
					if (mem.getUser().getId().equals(author.getId())) {
						return voi;
					}
				}
			}
		}
		return null;
	}

	/**
	 * Checks the given VoiceChannel for users which are not deafened and returns
	 * the result respectively.
	 * 
	 * @param voice channel to check for users
	 * @return true if there is any user in this channel and not deafened otherwise
	 *         false
	 */
	public static boolean checkUsersInVoiceChannel(VoiceChannel voice) {
		if (voice == null)
			return false;
		return voice.getMembers().stream().filter(m -> !m.getUser().isBot() && !m.getVoiceState().isDeafened()).count() > 0
				? true
				: false;// from jagrosh bot
	}

	/**
	 * Loads the given file as playlist with no validity checks done
	 * 
	 * @param playlist file to load
	 * @return loaded playlist as Vector<String>
	 */
	public static Vector<String> loadPlaylist(File playlist) {
		if (!playlist.exists()) {
			return null;
		}

		Vector<String> ret = new Vector<>();
		try {
			FileReader fr = null;
			BufferedReader bfr = null;
			fr = new FileReader(playlist);
			bfr = new BufferedReader(fr);
			String chk;
			while ((chk = bfr.readLine()) != null) {

				if (chk.equals("#shuffle") || chk.equals("//shuffle"))
					continue;// be compatible with local playlist file from jagroshs bot

				ret.add(chk);
			}
			bfr.close();
		} catch (IOException e1) {
			e1.printStackTrace();
			return null;
		}

		return ret;
	}

	/**
	 * Shuffles the passed list with the Fisher Yates algorithm.
	 * 
	 * @param list to shuffle
	 * @return the same list for chaining
	 */
	public static <E> List<E> fisherYatesShuffle(List<E> list) {
		if (list == null || list.size() < 2)
			return list;// we don't need to shuffle this list

		E save;
		int j;
		SecureRandom sr = new SecureRandom();
		for (int i = 0; i < list.size(); i++) {
			save = list.get(i);
			j = sr.nextInt(list.size());
			if (i != j) {
				list.set(i, list.get(j));
			}
			list.set(j, save);
		}
		return list;
	}

	/**
	 * Converts milliseconds into its hh:mm:ss String representation
	 * 
	 * @param mills time in milliseconds
	 * @return A String representation like hh:mm:ss of the time
	 */
	public static String millsToHourMinuteSecond(long mills) {
		long hr, min;
		StringBuffer ret = new StringBuffer();

		if ((hr = ((mills / 1000) / 3600)) != 0) {
			ret.append(hr + ":");
		}

		if ((min = (((mills / 1000) % 3600) / 60)) != 0) {
			ret.append(String.format("%02d", min) + ":");
		}
		ret.append(String.format("%02d", (Math.round(mills / 1000f) % 60)));

		return ret.toString();
	}

}
