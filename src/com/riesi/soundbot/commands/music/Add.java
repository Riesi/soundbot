package com.riesi.soundbot.commands.music;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class Add extends Command {

	public Add() {
		this.name="add";
		this.help="Adds a given link to the autoplaylist.";
		this.arguments=" [URL]";
		this.mode=BotMode.music;
		this.argumentCnt=2;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if(new File(args[1]).exists()) {
			messageEv.getChannel().sendMessage("**Not** a valid track.").submit();
			return false;//block adding of local files for "reasons"
		}
		
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		VoiceChannel voi = Utilities.findUserInVoice(messageEv.getGuild(), messageEv.getAuthor());
		if(voi!=null) {
			String serverId = messageEv.getGuild().getId();
			ins.playCall(messageEv.getGuild().getId(), voi, args[1], false, new CommandCallback(serverId, args[1]) {
				@Override
				public void callBackCommand(String info) {
					if(info == null) {
						messageEv.getChannel().sendMessage("**Not** a valid track.").submit();
						return;
					}
					//Since link is valid add it!
					messageEv.getChannel().sendMessage("Adding track!").submit();
					File playlist = new File(MainSoundboard.plsPath, "playlist_"+serverId+".txt");
					FileWriter plWriter = null;
					BufferedReader plReader = null;
					try {
						plWriter = new FileWriter(playlist,true);	
						plReader = new BufferedReader(new FileReader(playlist));
					} catch (IOException e) {
						messageEv.getChannel().sendMessage("Could **not** interact with playlist!").submit();
					}
				  String chk="";
				  try {
						while((chk=plReader.readLine()) != null) {
							 if(chk.contains(args[1])) {
								 messageEv.getChannel().sendMessage("Track **already** in playlist!").submit();
								 return;
							 }
						}
					} catch (IOException e1) {
						messageEv.getChannel().sendMessage("Could **not** read the playlist!").submit();
					}
				  
				try {
					plWriter.append(args[1]+"\n");
					plWriter.flush();
					plWriter.close();
				} catch (IOException e) {
					messageEv.getChannel().sendMessage("Could **not** write the playlist!").submit();
				}
					
				}
			});
		}else {
			messageEv.getChannel().sendMessage("Could not find you in voice channels!").submit();
		}
		return false;
	}

}
