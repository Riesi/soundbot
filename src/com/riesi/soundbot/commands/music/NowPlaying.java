package com.riesi.soundbot.commands.music;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.HaltCatchFire;
import com.riesi.soundbot.util.TrackInfo;
import com.riesi.soundbot.util.Utilities;
import com.sedmelluq.discord.lavaplayer.source.local.LocalAudioTrack;
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioTrack;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class NowPlaying extends Command {

	public NowPlaying() {
		this.name="np";
		this.mode=BotMode.music;
		this.help="Presents the current playing track of the server.";
		this.permissionLevel = BotPermission.user;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		try {
			TrackInfo tif = RevampContainer.GET_INSTANCE().currentTrack(messageEv.getGuild().getId());
		
			StringBuffer sbf = new StringBuffer();
			EmbedBuilder infoBuilder = new EmbedBuilder();
			if(tif.getAudioTrack() instanceof LocalAudioTrack) {//should be local
				if(tif.getAudioTrack().getInfo().title.equals("Unknown title")) {
					infoBuilder.setAuthor(tif.getAudioTrack().getInfo().uri);
			  }else {
			  	infoBuilder.setAuthor(tif.getAudioTrack().getInfo().title);
			  }
			}else {//should be online
				infoBuilder.setAuthor(tif.getAudioTrack().getInfo().title, tif.getAudioTrack().getInfo().uri);
			}

			sbf.append("**❙");
			long pos = (Utilities.nowPlayingBars*tif.getPosition())/tif.getDuration();
			for(int i=0;i<Utilities.nowPlayingBars;i++) {
				if(pos==i) sbf.append("⬢");
				else sbf.append("━");
			}
			sbf.append("❙ **");
			
			infoBuilder.setDescription(sbf.toString());
			infoBuilder.setColor(messageEv.getGuild().getSelfMember().getColor());
			infoBuilder.setFooter(Utilities.millsToHourMinuteSecond(tif.getPosition())
					+"/"+Utilities.millsToHourMinuteSecond(tif.getDuration()), null);
			
			//if this is a Track from youtube we can get a neat picture!
			if(tif.getAudioTrack() instanceof YoutubeAudioTrack) {
				infoBuilder.setThumbnail("https://i.ytimg.com/vi/"+tif.getAudioTrack().getIdentifier()+"/mqdefault.jpg");
			}
			messageEv.getChannel().sendMessage(infoBuilder.build()).submit();
			return true;
		} catch (HaltCatchFire e) {
			messageEv.getChannel().sendMessage("No track found.").submit();
			return false;
		}
	}

}
