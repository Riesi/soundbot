package com.riesi.soundbot.commands.music;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Flush extends Command {

	public Flush() {
		this.name="flush";
		this.help="Flushes queue, but current track keeps playing.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.moderator;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		if(ins.flush(messageEv.getGuild().getId())) {
			messageEv.getChannel().sendMessage("Queue flushed!").submit();
		}else {
			messageEv.getChannel().sendMessage("Queue could **not** be flushed!").submit();
		}
		return true;
	}

}
