package com.riesi.soundbot.commands.music;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Delete extends Command {

	public Delete() {
		this.name="del";
		this.help="Delets a given link from the corresponding server playlist.";
		this.arguments=" [URL]";
		this.mode=BotMode.music;
		this.argumentCnt=2;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		String serverId = messageEv.getGuild().getId();
		File playlist = new File(MainSoundboard.plsPath, "playlist_"+serverId+".txt");
		
		if(!playlist.exists()) {
			messageEv.getChannel().sendMessage("**No** playlist avaliable on this server!").submit();
			return false;
		}
		
    FileReader fr = null;
    BufferedReader bfr=null;
    try {
			fr= new FileReader(playlist);
			bfr = new BufferedReader(fr);
		} catch (IOException e) {
			messageEv.getChannel().sendMessage("Could **not** read the playlist!").submit();
			return false;
		}
    
    String chk="";
    StringBuffer bf= new StringBuffer();
    boolean got=false;
    try {
			while((chk=bfr.readLine()) != null) {

				 if(chk.contains(args[1])) {
						got=true;
				 }else {
					 bf.append(chk+"\n");
				 }
			 }
			bfr.close();
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
    
	  if(got) {
		  try {
		  	FileWriter fw = new FileWriter(playlist,false);	
				fw.append(bf.toString());
				fw.flush();
		    fw.close();
			} catch (IOException e) {
				messageEv.getChannel().sendMessage("Error **writing** playlist!").submit();
			}
		  messageEv.getChannel().sendMessage("Track **removed** from playlist!").submit();
	  }else {
	  	messageEv.getChannel().sendMessage("Track **not** in playlist!").submit();
	  }
		return true;
	}

}
