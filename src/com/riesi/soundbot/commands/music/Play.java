package com.riesi.soundbot.commands.music;


import java.io.File;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;


public class Play extends Command {

	public Play() {
		this.name="play";
		this.help="Starts playing the autoplaylist or a given URL or the corresponding server playlist.";
		this.arguments=" [null|URL] or [playlist|shuffle] [null|NAME]";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.user;
		this.argumentCnt=1;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		String serverId = messageEv.getGuild().getId();
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		VoiceChannel voi = Utilities.findUserInVoice(messageEv.getGuild(), messageEv.getAuthor());
		
		if(args.length==1) {
			if(voi!=null) {
				ins.playAuto(serverId, voi);
				return true;
			}else {
				return false;
			}
		}
		
		if(new File(args[1]).exists()) {
			messageEv.getChannel().sendMessage("**Not** a valid track.").submit();
			return false;//block playing of local files for "reasons"
		}
		
		
		if(voi!=null) {
			
			//play playlist if moderator dictates
			playListlabel:
			if(perm.getPermissionValue()>BotPermission.user.getPermissionValue()) {
				Boolean playlistMode=null;
				switch (args[1]) {
				case "playlist":
					playlistMode=true;
					break;
	
				case "shuffle":
					playlistMode=false;
					break;
	
				default:
					break playListlabel;
				}
				
				//setup playlist
				File playlist=null;
				if((args.length>2)) {
					if((playlist = new File(MainSoundboard.plsPath,args[2]+".txt")).exists()) {
						//do nothing ok!
					}else {
						messageEv.getChannel().sendMessage("Playlist does not exist!").submit();
						return false;
					}
				}else{
					playlist = new File(MainSoundboard.plsPath,"playlist_"+serverId+".txt");
				}
				if(playlistMode) {//straight
					ins.playList(serverId, voi, Utilities.loadPlaylist(playlist), new CommandCallback(serverId, playlist.getName()) {
						@Override
						public void callBackCommand(String info) {
							if(info == null) {
								messageEv.getChannel().sendMessage("**Error** loading playlist.").submit();
								return;
							}
							messageEv.getChannel().sendMessage("Started playlist!").submit();
						}
					});
					return true;
				}else {//shuffle
					ins.playList(serverId, voi, Utilities.fisherYatesShuffle(
							Utilities.loadPlaylist(playlist)), new CommandCallback(serverId, args[1]) {
						@Override
						public void callBackCommand(String info) {
							if(info == null) {
								messageEv.getChannel().sendMessage("**Error** loading playlist.").submit();
								return;
							}
							messageEv.getChannel().sendMessage("Started shuffled playlist!").submit();
						}
					});
					return true;
				}				
			}
			
			//play this as track since its not a 2nd command
			ins.playCall(serverId, voi, args[1],false, new CommandCallback(serverId, args[1]) {
				@Override
				public void callBackCommand(String info) {
					if(info == null) {
						messageEv.getChannel().sendMessage("**Not** a valid track.").submit();
						return;
					}
					messageEv.getChannel().sendMessage("Added track to play!").submit();
				}
			});
		}else {
			messageEv.getChannel().sendMessage("Could not find you in voice channels!").submit();
		}
		return false;
	}

}
