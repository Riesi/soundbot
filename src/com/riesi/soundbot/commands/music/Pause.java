package com.riesi.soundbot.commands.music;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Pause extends Command {

	public Pause() {
		this.name="pause";
		this.help="Pauses the current track.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.moderator;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		if(ins.pause(messageEv.getGuild().getId(),true)) {
			messageEv.getChannel().sendMessage("Player **succesfully** paused!").submit();
		}else {
			messageEv.getChannel().sendMessage("Could **not** pause player!").submit();
		}
		return true;
	}

}
