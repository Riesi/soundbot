package com.riesi.soundbot.commands.music;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Resume extends Command {

	public Resume() {
		this.name="resume";
		this.help="Resumes the current track.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.user;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		if(ins.pause(messageEv.getGuild().getId(),false)) {
			messageEv.getChannel().sendMessage("Player **succesfully** resumed!").submit();
		}else {
			messageEv.getChannel().sendMessage("Could **not** resume player!").submit();
		}
		return true;
	}

}
