package com.riesi.soundbot.commands.music;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.managers.AudioManager;


public class Stop extends Command {

	public Stop() {
		this.name="stop";
		this.help="Stops playing and flushes queue.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.moderator;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		AudioManager man = messageEv.getGuild().getAudioManager();
		if(man!=null&&ins.stop(messageEv.getGuild().getId(),man)) {
			messageEv.getChannel().sendMessage("**Successfully** stopped player and flushed queue!").submit();
		}else {
			messageEv.getChannel().sendMessage("Could **not** stop player!").submit();
		}

		return true;
	}

}
