package com.riesi.soundbot.commands.music;

import java.io.File;
import java.util.Iterator;
import java.util.Vector;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

public class Playlists extends Command {

	public Playlists() {
		this.name="playlists";
		this.help="Prints the avaliable playlists.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.moderator;
		this.privateUse=true;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		Iterator<File> files = FileUtils.listFiles(MainSoundboard.plsPath, 
				new String[]{"txt"}, false).iterator();
		File nex;
		if(!files.hasNext()) {
			messageEv.getChannel().sendMessage("**No** playlists avaliable!").submit();
			return false;
		}
		Vector<String> outputBuffer= new Vector<>();
		StringBuffer bf = new StringBuffer();
		String sf;
		
		EmbedBuilder infoBuilder = new EmbedBuilder();
		infoBuilder.setTitle("The following playlists are available:\n");
		infoBuilder.setColor(messageEv.getGuild().getSelfMember().getColor());
		while(files.hasNext()) {
			nex = files.next();
			sf = FilenameUtils.getBaseName(nex.getName());
			if((bf.length()+sf.length())<Utilities.characterLimit) {
				bf.append(sf+"\n");
			}else {
				outputBuffer.add(bf.toString());
				bf = new StringBuffer();
			}
		}
		outputBuffer.add(bf.toString());
		messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(0)).build()).submit();
		infoBuilder.setTitle(null);
		for(int i=1; i<outputBuffer.size();i++) {
			messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(i)).build()).submit();
		}
		return true;
	}

}
