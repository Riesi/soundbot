package com.riesi.soundbot.commands.music;

import java.io.File;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class ForcePlay extends Command {

	public ForcePlay() {
		this.name="forceplay";
		this.help="Plays a given link and skips the current playing track.";
		this.arguments=" [URL]";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.moderator;
		this.argumentCnt=2;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
if(new File(args[1]).exists())return false;//block playing of local files for "reasons"
		
RevampContainer ins = RevampContainer.GET_INSTANCE();
		VoiceChannel voi = Utilities.findUserInVoice(messageEv.getGuild(), messageEv.getAuthor());
		if(voi!=null) {
			String serverId = messageEv.getGuild().getId();
			ins.playCall(serverId, voi, args[1],true, new CommandCallback(serverId, args[1]) {
				@Override
				public void callBackCommand(String info) {
					if(info == null) {
						messageEv.getChannel().sendMessage("**Not** a valid track.").submit();
						return;
					}
					messageEv.getChannel().sendMessage("Track playing now!").submit();
				}
			});
		}else {
			messageEv.getChannel().sendMessage("Could not find you in voice channels!").submit();
		}
		return false;
	}

}
