package com.riesi.soundbot.commands.music;

import java.io.File;
import java.util.Vector;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

public class Queue extends Command {

	public Queue() {
		this.name="queue";
		this.help="Returns the currently queued tracks.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.user;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		//Retrieve queue
		Vector<String[]> qu = RevampContainer.GET_INSTANCE().getQueue(messageEv.getGuild().getId());
		if(qu==null) {
			messageEv.getChannel().sendMessage("Currently **no** player running on server!").submit();
			return false;
		}
		if(qu.size()==0) {
			messageEv.getChannel().sendMessage("Currently **no** track queued!").submit();
			return false;
		}
		
		//generate queue infos
		Vector<String> outputBuffer= new Vector<>();
		StringBuffer bf = new StringBuffer();
		for(var qt: qu) {
			if(qt[0].length()>62) {//I bet this is extremely slow but anyway...
				qt[0]=qt[0].substring(0, 59).concat("...");
			}
			if(qt[0].equals("Unknown title")) {
				File track =new File(qt[1]);
				if(track.exists()) {//don't expose unnecessary path
					setTitle("`"+track.getName()+"`\n------------------------------\n", bf, outputBuffer);
				}else {
					//since we don't have more infos show only this
					setTitle("`"+qt[1]+"`\n------------------------------\n", bf, outputBuffer);
				}
		  }else {
		  	setTitle("`"+qt[0]+"`\n<"+qt[1]+">\n", bf, outputBuffer);
		  }
		}
		outputBuffer.add(bf.toString());
		
		//present the queue info
		EmbedBuilder infoBuilder = new EmbedBuilder();
		infoBuilder.setTitle("Currently queued tracks:\n");
		infoBuilder.setColor(messageEv.getGuild().getSelfMember().getColor());
		messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(0)).build()).submit();
		infoBuilder.setTitle(null);
		for(int i=1; i<outputBuffer.size();i++) {
			messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(i)).build()).submit();
		}
		return true;
	}
	
	private void setTitle(String title, StringBuffer bf, Vector<String> outputBuffer) {
		if((bf.length()+title.length())<Utilities.characterLimit) {
			bf.append(title);
		}else {
			outputBuffer.add(bf.toString());
			bf = new StringBuffer();
		}
	}
}
