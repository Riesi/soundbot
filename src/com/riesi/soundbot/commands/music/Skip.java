package com.riesi.soundbot.commands.music;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.commands.callbacks.CommandCallback;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class Skip extends Command {
	private Map<String, Vector<String>> votes= new HashMap<>();

	public Skip() {
		this.name="skip";
		this.help="Skips the current track, if the majority of the channel agrees.";
		this.mode=BotMode.music;
		this.permissionLevel = BotPermission.user;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		//retrieve voice channel
		VoiceChannel voi = messageEv.getGuild().getSelfMember().getVoiceState().getChannel();
		if(voi==null) {
			messageEv.getChannel().sendMessage("Not connected to any voice channel!").submit();
			return false;
		}
		//check if permission is higher than user
		if(perm.getPermissionValue()>=BotPermission.moderator.getPermissionValue()) {
			return doSkip(messageEv);
		}else {
			String serverId = messageEv.getGuild().getId();
			//I dont know if this is a good solution, but hey it works...
			RevampContainer.GET_INSTANCE().registerSkip(serverId, new CommandCallback(serverId,null) {
				@Override
				public void callBackCommand(String info) {
					System.out.println("flush skip votes!");
					votes.remove(this.serverId);
				}
			});
			Vector<String> vec = votes.get(serverId);
			int needed= ((int)voi.getMembers().stream()
          .filter(m -> !m.getUser().isBot() && !m.getVoiceState().isDeafened()).count()/2)+1;//from jagrosh bot
		
			//add users to vote list an
			if(vec==null) {
				vec = new Vector<>();
				vec.add(messageEv.getAuthor().getId());
				votes.put(serverId, vec);
			}else {
				if(vec.contains(messageEv.getAuthor().getId())) {
					messageEv.getChannel().sendMessage("You **already** voted!").submit();
					return false;
				}else {
					vec.add(messageEv.getAuthor().getId());
				}
			}
			//check if enough users voted for skip
			if(needed<=vec.size()) {
				return doSkip(messageEv);
			}else {
				messageEv.getChannel().sendMessage("Current skip status **"+vec.size()+" of "+needed+"** needed votes.").submit();
			}
		}
		return false;
	}
	
	/**
	 * Skip and clear vote list
	 * @param messageEv
	 * @return always true
	 */
	private boolean doSkip(Message messageEv) {
		votes.remove(messageEv.getGuild().getId());
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		if(ins.skip(messageEv.getGuild().getId())) {
			messageEv.getChannel().sendMessage("Skipping current track!").submit();
		}else {
			messageEv.getChannel().sendMessage("Nothing to skip!").submit();
		}

		return true;
	}
}
