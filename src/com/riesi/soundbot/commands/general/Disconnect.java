package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.managers.AudioManager;

public class Disconnect extends Command {

	public Disconnect() {
		this.name="disconnect";
		this.help="Disconnects the bot from the current server.";
		this.permissionLevel=BotPermission.admin;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		AudioManager man = messageEv.getGuild().getAudioManager();
		if(man!=null&&ins.stop(messageEv.getGuild().getId(),man)) {
			messageEv.getChannel().sendMessage("**Successfully** disconnected!").submit();
		}else {
			messageEv.getChannel().sendMessage("Could **not** disconnect!").submit();
		}

		return true;
	}

}
