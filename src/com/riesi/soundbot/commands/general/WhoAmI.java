package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class WhoAmI extends Command {

	public WhoAmI() {
		this.name="whoami";
		this.help="Tells you your permission level.";
		this.permissionLevel=BotPermission.user;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		messageEv.getChannel().sendMessage(messageEv.getAuthor().getAsMention()+" your permission level is **"+perm.toString()+"**.").submit();
		return true;
	}

}
