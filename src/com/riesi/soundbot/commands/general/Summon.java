package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;

public class Summon extends Command {

	public Summon() {
		this.name = "summon";
		this.help = "Summons the bot in your voice channel.";
		this.permissionLevel = BotPermission.user;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		VoiceChannel voi = Utilities.findUserInVoice(messageEv.getGuild(), messageEv.getAuthor());
		if (voi != null) {
			ins.connectVoiceChannel(messageEv.getGuild().getId(), voi);
			return true;
		} else {
			messageEv.getChannel().sendMessage("Could not find you in voice channels!").submit();
		}
		return false;
	}

}
