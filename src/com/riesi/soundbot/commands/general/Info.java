package com.riesi.soundbot.commands.general;


import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;


public class Info extends Command {

	public Info() {
		this.name="info";
		this.help="Github and creator info about the bot.";
		this.permissionLevel = BotPermission.user;
		this.privateUse=true;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		EmbedBuilder infoBuilder = new EmbedBuilder();
		infoBuilder.setAuthor("SoundBot","https://github.com/Riesi/SoundBot");
		infoBuilder.setDescription("A music/soundboard bot for discord! Written in Java with JDA and Lavaplayer.");
		infoBuilder.setColor(849895);
//		infoBuilder.setFooter("`3:23/4:16`", null);
		infoBuilder.setThumbnail("https://avatars0.githubusercontent.com/u/27691485");
		
		messageEv.getChannel().sendMessage(infoBuilder.build()).submit();
		return true;
	}

}
