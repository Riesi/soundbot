package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;

public class ClearMessages extends Command {

	public ClearMessages() {
		this.name = "clearmsg";
		this.help = "Deletes messages sent by the bot from the last 100 messages in the corresponding channel.";
		this.permissionLevel = BotPermission.admin;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		MessageHistory mh;
		long botId = messageEv.getGuild().getSelfMember().getUser().getIdLong();
		mh = new MessageHistory(messageEv.getChannel());
		mh.retrievePast(100);
		for (Message m : mh.retrievePast(100).complete()) {
			if (m.getAuthor().getIdLong() == botId) {
				m.delete().submit();
			}
		}
		messageEv.getChannel().sendMessage("Cleaning everything up!").submit();
		return true;
	}

}
