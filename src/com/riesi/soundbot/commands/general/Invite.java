package com.riesi.soundbot.commands.general;


import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;

public class Invite extends Command {

	public Invite() {
		this.name="invite";	
		this.help="Sends an invite link.";
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		
		messageEv.getChannel().sendMessage(messageEv.getJDA().getInviteUrl(Utilities.serverPerms)).submit();
		return false;
	}

}
