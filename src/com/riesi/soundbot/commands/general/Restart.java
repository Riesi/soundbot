package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Restart extends Command {

	public Restart() {
		this.name="restart";
		this.help="Restarts the bot.";
		this.permissionLevel=BotPermission.moderator;
		this.privateUse=true;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		MainSoundboard.restart=true;
		RevampContainer.RESET();
		MainSoundboard.shutdownBot();
		return true;
	}

}
