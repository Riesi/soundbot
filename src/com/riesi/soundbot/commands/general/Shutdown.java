package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Shutdown extends Command {
	
	public Shutdown() {
		this.name="shutdown";
		this.help="Shuts the bot down.";
		this.privateUse=true;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		MainSoundboard.exitBot();
		return true;
	}
}
