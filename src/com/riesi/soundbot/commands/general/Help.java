package com.riesi.soundbot.commands.general;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;

import java.awt.*;
import java.util.Vector;

public class Help extends Command{

	public Help() {
		this.name="help";
		this.help="";
		this.permissionLevel = BotPermission.user;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		StringBuffer mess = new StringBuffer();
		mess.append("The bot is loaded as "+MainSoundboard.botConfig.getBotmode()+"-Bot.\n");
		Vector<Command> ownerCmd = new Vector<>();
		Vector<Command> adminCmd = new Vector<>();
		Vector<Command> modCmd = new Vector<>();
		Vector<Command> usrCmd = new Vector<>();

		for(var cmds: MainSoundboard.commands) {
			//if(cmds.getHelp().equals("")) continue;
			switch (cmds.getPermission()) {
			case owner:
				ownerCmd.add(cmds);
				break;
			case admin:
				adminCmd.add(cmds);
				break;
			case moderator:
				modCmd.add(cmds);
				break;
			case user:
				usrCmd.add(cmds);
				break;
			default:
				break;
			}
		}

		switch (perm){
			case owner: printCommandHelp(messageEv,ownerCmd,"**Owner commands:**",Color.CYAN);
			case admin: printCommandHelp(messageEv,adminCmd,"**Admin commands:**",Color.RED);
			case moderator:	printCommandHelp(messageEv,modCmd,"**Moderator commands:**",Color.ORANGE);
			case user: printCommandHelp(messageEv,usrCmd,"**User commands:**",Color.GREEN);
		}

		return true;
	}
	/**
	 * Generates a nice embed for the help info
	 * @param messageEv
	 * @param commands
	 * @param title
	 * @param c
	 */
	private void printCommandHelp(Message messageEv,Vector<Command> commands, String title, Color c) {
		Vector<String> outputBuffer= new Vector<>();
		StringBuffer bf = new StringBuffer();

		EmbedBuilder infoBuilder = new EmbedBuilder();
		infoBuilder.setTitle(title);
		infoBuilder.setColor(c);
		
		for(var cmds: commands) {
			if((bf.length())<Utilities.characterLimit) {
				bf.append("`"+MainSoundboard.botConfig.getPrefix()+cmds.getName()+cmds.getArguments()+"` - "+cmds.getHelp()+"\n");
			}else {
				outputBuffer.add(bf.toString());
				bf = new StringBuffer();
			}
		}
		outputBuffer.add(bf.toString());
		messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(0)).build()).submit();
		infoBuilder.setTitle(null);
		for(int i=1; i<outputBuffer.size();i++) {
			messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(i)).build()).submit();
		}
	}

}
