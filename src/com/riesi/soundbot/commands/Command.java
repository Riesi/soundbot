package com.riesi.soundbot.commands;

import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;

import net.dv8tion.jda.api.entities.Message;

public abstract class Command {
	protected String name="";
	
	protected String help="No help defined for command!";
	
	protected String arguments="";
	protected int argumentCnt=1;
	protected boolean modifiesCfg=false;
	protected boolean privateUse=false;

	protected BotMode mode = BotMode.always;
	protected BotPermission permissionLevel = BotPermission.admin;
	
	public abstract boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg);
	
	public String getName(){
        return name;
    }
	
	public String getHelp(){
        return help;
    }
	
	public BotMode getMode() {
		return mode;
	}
	
	public int getArgumentCnt() {
		return argumentCnt;
	}
	
	public String getArguments() {
		return arguments;
	}
	public BotPermission getPermission() {
		return permissionLevel;
	}
	
	public boolean modifiesCfg() {
		return modifiesCfg;
	}
	public boolean isPrivateUse() {
		return privateUse;
	}

	public void setPrivateUse(boolean privateUse) {
		this.privateUse = privateUse;
	}
}
