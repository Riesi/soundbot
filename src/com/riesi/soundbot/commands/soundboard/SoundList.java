package com.riesi.soundbot.commands.soundboard;

import java.io.File;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Vector;

import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;


public class SoundList extends Command {

	public SoundList() {
		this.name = "soundlist";
		this.help = "Lists every sound file to play.";
		this.mode = BotMode.soundboard;
		this.permissionLevel = BotPermission.user;
		this.privateUse = true;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		Iterator<File> files = FileUtils.listFiles(MainSoundboard.audioPath, Utilities.audioExtensions, false).iterator();
		if (!files.hasNext()) {
			messageEv.getChannel().sendMessage("**No** sound files in the audio folder!").submit();
			return false;
		}

		TreeSet<String> sortedFiles = new TreeSet<String>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);// overwrite comparator for case ignoring sort
			}
		});

		while (files.hasNext()) {// TreeSet is a sorted set
			sortedFiles.add(FilenameUtils.getBaseName(files.next().getName()));
		}

		Vector<String> outputBuffer = new Vector<>();
		StringBuffer bf = new StringBuffer();
		String sf;

		EmbedBuilder infoBuilder = new EmbedBuilder();
		infoBuilder.setTitle("The following sound files are available:\n");
		infoBuilder.setColor(messageEv.getGuild().getSelfMember().getColor());
		for (var fileName : sortedFiles) {
			sf = FilenameUtils.getBaseName(fileName);
			if ((bf.length() + sf.length()) < Utilities.characterLimit) {
				bf.append(sf + "\n");
			} else {
				outputBuffer.add(bf.toString());
				bf = new StringBuffer();
			}
		}
		outputBuffer.add(bf.toString());
		messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(0)).build()).submit();
		infoBuilder.setTitle(null);
		for (int i = 1; i < outputBuffer.size(); i++) {
			messageEv.getChannel().sendMessage(infoBuilder.setDescription(outputBuffer.get(i)).build()).submit();
		}

		return true;
	}

}
