package com.riesi.soundbot.commands.soundboard;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;


public class SoundBoard extends Command {

	private HashMap<String, Long> cooldown = new HashMap<>();
	
	public SoundBoard() {
		this.name="sb";
		this.help="Plays the specified sound file.";
		this.arguments=" [SOUNDFILE]";
		this.mode=BotMode.soundboard;
		this.permissionLevel=BotPermission.user;
		this.argumentCnt=2;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		//only users can have cooldown
		if(perm.equals(BotPermission.user)&&(serverCfg.getCooldown()>0)&&cooldown.containsKey(messageEv.getAuthor().getId())) {
			long time= cooldown.get(messageEv.getAuthor().getId());
			if((java.lang.System.currentTimeMillis()-time)>(serverCfg.getCooldown()*1000)){
				cooldown.remove(messageEv.getAuthor().getId());
			}else {
				messageEv.getChannel().sendMessage("Sorry you are on **cooldown**!").submit();
				return false;
			}
		}
		
		//go through the audio files
		Iterator<File> files = FileUtils.listFiles(MainSoundboard.audioPath, 
				Utilities.audioExtensions, false).iterator();
		File nex;
		File soundfile = null;
		while(files.hasNext()) {
			nex = files.next();
			if(FilenameUtils.getBaseName(nex.getName()).equalsIgnoreCase(args[1])) {
				soundfile=new File(MainSoundboard.audioPath, nex.getName());
				break;
			}
		}
		
		if(soundfile==null||!soundfile.exists()) {
			messageEv.getChannel().sendMessage("Sound file does not exist!").submit();
			return false;
		}
		
		//play the file
		RevampContainer ins = RevampContainer.GET_INSTANCE();
		VoiceChannel voi = Utilities.findUserInVoice(messageEv.getGuild(), messageEv.getAuthor());
		if(voi!=null) {
			ins.play(messageEv.getGuild().getId(), voi, soundfile.getPath(),true);
			if(perm.equals(BotPermission.user)&&(serverCfg.getCooldown()>0)){
				cooldown.put(messageEv.getAuthor().getId(), java.lang.System.currentTimeMillis());
			}
		}else {
			messageEv.getChannel().sendMessage("Could not find you in voice channels!").submit();
		}
		return false;
	}

}
