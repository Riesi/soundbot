package com.riesi.soundbot.commands.soundboard;

import java.io.File;

import net.dv8tion.jda.api.entities.Message;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

public class AddSound extends Command {

	public AddSound() {
		this.name="addsound";
		this.help="Adds the sound file of the attachment to the audio list.";
		this.mode=BotMode.soundboard;
		this.privateUse=true;
		this.permissionLevel=BotPermission.admin;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if(!messageEv.getAttachments().isEmpty()) {
			Message.Attachment att =messageEv.getAttachments().get(0);
			if(FilenameUtils.isExtension(att.getFileName(), Utilities.audioExtensions)) {
				att.downloadToFile(new File(MainSoundboard.audioPath,att.getFileName()));
				messageEv.getChannel().sendMessage("Sound successfully **added**!").submit();
				return true;
			}else {
				messageEv.getChannel().sendMessage("Attached file is **no sound** file!").submit();
			}	
		}else {
			messageEv.getChannel().sendMessage("**No** attachment found!").submit();
		}
		return false;
	}

}
