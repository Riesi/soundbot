package com.riesi.soundbot.commands.soundboard;

import java.io.File;
import java.util.Random;
import java.util.Vector;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.apache.commons.io.FileUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

public class SetRandomSound extends Command {

	private static Thread random = null;

	public SetRandomSound() {
		this.name = "randomsound";
		this.help = "Sets if the bot should play random sounds or not.";
		this.arguments = " [false|true]";
		this.permissionLevel = BotPermission.admin;
		this.modifiesCfg = true;
		this.privateUse = true;
		this.argumentCnt = 1;

		randomPlay();
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if (args.length >= 2) {
			switch (args[1]) {
			case "true":
				MainSoundboard.botConfig.getSoundboard().setRandomSound(true);
				randomPlay();
				break;

			case "false":
				MainSoundboard.botConfig.getSoundboard().setRandomSound(false);
				break;
			default:
				messageEv.getChannel().sendMessage("**Not** a vaild value!").submit();
				return false;
			}
		} else {
			messageEv.getChannel()
					.sendMessage("Random play is **" + MainSoundboard.botConfig.getSoundboard().isRandomSound() + "**.").submit();
		}
		return true;
	}

	private void randomPlay() {
		if (MainSoundboard.botConfig.getSoundboard().isRandomSound()) {
			Vector<File> soundboardFiles = new Vector<>(
					FileUtils.listFiles(MainSoundboard.audioPath, Utilities.audioExtensions, false));

			random = new Thread(new Runnable() {
				long cnt;
				Random rnd = new Random();

				@Override
				public void run() {
					while (true) {
						cnt = 0;
						while (MainSoundboard.botConfig.getSoundboard().isRandomSound()
								&& (MainSoundboard.botConfig.getSoundboard().getDelayTime() - cnt) > 0) {
							try {
								Thread.sleep(10000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							cnt += 10;
						}
						if (!MainSoundboard.botConfig.getSoundboard().isRandomSound())
							return;
						if (!((rnd.nextInt(100)) <= MainSoundboard.botConfig.getSoundboard().getProbability()))
							continue;

						RevampContainer ins = RevampContainer.GET_INSTANCE();
						VoiceChannel voi = null;
						int index = rnd.nextInt(soundboardFiles.size());
						for (var guild : MainSoundboard.jda.getGuilds()) {
							voi = null;
							voi = Utilities.findUserInVoice(guild, MainSoundboard.jda.getSelfUser());
							if (voi != null) {
								ins.play(guild.getId(), voi, soundboardFiles.get(index).getPath(), true);
							}
						}
					}
				}
			});
			random.start();
		}
	}
}
