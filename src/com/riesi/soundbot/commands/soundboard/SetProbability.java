package com.riesi.soundbot.commands.soundboard;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class SetProbability extends Command {

	public SetProbability() {
		this.name = "probability";
		this.help = "Sets the probability for the bot to play a random sound in percent.";
		this.arguments = " [0...100]";
		this.permissionLevel = BotPermission.admin;
		this.modifiesCfg = true;
		this.privateUse = true;
		this.argumentCnt = 1;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if (args.length >= 2) {
			try {
				int cd = Integer.parseInt(args[1]);
				cd = cd < 0 ? 0 : cd > 100 ? 100 : cd;// limit
				MainSoundboard.botConfig.getSoundboard().setProbability(cd);

				messageEv.getChannel()
						.sendMessage("Probability set to **" + MainSoundboard.botConfig.getSoundboard().getProbability() + "%**.")
						.submit();
				return true;
			} catch (NumberFormatException e) {
				messageEv.getChannel().sendMessage("Invalid argument not a number").submit();
			}
		} else {
			messageEv.getChannel()
					.sendMessage("Probability is **" + MainSoundboard.botConfig.getSoundboard().getProbability() + "%**.")
					.submit();
		}
		return false;
	}

}
