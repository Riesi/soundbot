package com.riesi.soundbot.commands.soundboard;

import java.io.File;
import java.util.Iterator;

import net.dv8tion.jda.api.entities.Message;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

public class RemoveSound extends Command {

	public RemoveSound() {
		this.name="rmsound";
		this.help="Removes the sound file from the audio list.";
		this.mode=BotMode.soundboard;
		this.argumentCnt=2;
		this.privateUse=true;
		this.permissionLevel=BotPermission.admin;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		Iterator<File> files = FileUtils.listFiles(MainSoundboard.audioPath, 
				Utilities.audioExtensions, false).iterator();
		File nex;
		while(files.hasNext()) {
			nex = files.next();
			if(FilenameUtils.getBaseName(nex.getName()).equals(args[1])) {
				nex.delete();
				messageEv.getChannel().sendMessage("Sound successfully **deleted**!").submit();
				return true;
			}
		}
			
		messageEv.getChannel().sendMessage("**No sound** file with name found!").submit();
	
		return false;
	}

}
