package com.riesi.soundbot.commands.soundboard;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;
import net.dv8tion.jda.api.entities.Message;

public class SetDelayTime extends Command {

	public SetDelayTime() {
		this.name = "delaytime";
		this.help = "Sets the delay time between random sounds in seconds.";
		this.arguments = " [" + Utilities.minimumDelay + "..." + Long.MAX_VALUE + "]";
		this.permissionLevel = BotPermission.admin;
		this.modifiesCfg = true;
		this.privateUse = true;
		this.argumentCnt = 1;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if (args.length >= 2) {
			try {
				long cd = Long.parseLong(args[1]);
				cd = cd < Utilities.minimumDelay ? Utilities.minimumDelay : cd > Long.MAX_VALUE ? Long.MAX_VALUE : cd;// limit
																																																							// cooldown
				MainSoundboard.botConfig.getSoundboard().setDelayTime(cd);

				messageEv.getChannel()
						.sendMessage("Delay time set to **" + MainSoundboard.botConfig.getSoundboard().getDelayTime() + "s**.")
						.submit();
				return true;
			} catch (NumberFormatException e) {
				messageEv.getChannel().sendMessage("Invalid argument not a number").submit();
			}

		} else {
			messageEv.getChannel()
					.sendMessage("Current delay time is **" + MainSoundboard.botConfig.getSoundboard().getDelayTime() + "s**.")
					.submit();
		}
		return false;
	}
}
