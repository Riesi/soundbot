package com.riesi.soundbot.commands.callbacks;

public abstract class CommandCallback {
	public final String serverId;
	protected String argument;
	
	public abstract void callBackCommand(String info);
	
	public CommandCallback(String serverId, String argument) {
		this.serverId = serverId;
		this.argument = argument;
	}
	public void setArg(String arg) {
		argument=arg;
	}
	public String getArg() {
		return argument;
	}
}
