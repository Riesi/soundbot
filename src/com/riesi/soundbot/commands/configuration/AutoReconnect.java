package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class AutoReconnect extends Command {

	public AutoReconnect() {
		this.name="autoreconnect";
		this.help="Sets the autoreconnect value.";
		this.arguments=" [true|false]";
		this.argumentCnt=2;
		this.permissionLevel=BotPermission.owner;
		this.modifiesCfg=true;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		switch (args[1]) {
		case "true":
			MainSoundboard.botConfig.setAutoReconnect(true);
			break;

		case "false":
			MainSoundboard.botConfig.setAutoReconnect(false);
			break;
		default:
			messageEv.getChannel().sendMessage("**Not** a vaild value!").submit();
			return false;
		}
		MainSoundboard.jda.setAutoReconnect(MainSoundboard.botConfig.isAutoReconnect());
		messageEv.getChannel().sendMessage("Autoreconnect changed to **"+MainSoundboard.botConfig.isAutoReconnect()+"**!").submit();
		return true;
	}

}
