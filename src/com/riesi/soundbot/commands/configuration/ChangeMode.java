package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class ChangeMode extends Command {

	public ChangeMode() {
		this.name="changemode";
		this.help="Changes the mode of the bot.";
		this.arguments=" [soundboard|music|debug]";
		this.argumentCnt=2;
		this.permissionLevel=BotPermission.owner;
		this.modifiesCfg=true;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		switch (args[1]) {
		case "soundboard":
			MainSoundboard.botConfig.setBotmode(BotMode.soundboard);
			break;

		case "music":
			MainSoundboard.botConfig.setBotmode(BotMode.music);
			break;
		case "debug":
			MainSoundboard.botConfig.setBotmode(BotMode.debug);
			break;
		default:
			messageEv.getChannel().sendMessage("**Not** a vaild mode!").submit();
			return false;
		}
		MainSoundboard.orderCommands();
		messageEv.getChannel().sendMessage("Mode changed to **"+MainSoundboard.botConfig.getBotmode()+"**!").submit();
		return true;
	}

}
