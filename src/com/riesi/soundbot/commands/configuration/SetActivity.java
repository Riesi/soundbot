package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.Activity;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;

import net.dv8tion.jda.api.entities.Message;

public class SetActivity extends Command {

	public SetActivity() {
		this.name="activity";
		this.help="Sets the status activity.";
		this.arguments=" [listening|playing|watching]";
		this.argumentCnt=2;
		this.permissionLevel=BotPermission.owner;
		this.modifiesCfg=true;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		switch (args[1]) {
		case "listening":
			MainSoundboard.botConfig.setActivity(Activity.listening);
			break;

		case "playing":
			MainSoundboard.botConfig.setActivity(Activity.playing);
			break;
			
		case "watching":
			MainSoundboard.botConfig.setActivity(Activity.watching);
			break;
		case "streaming"://since you can't do cool stuff with the stream links...
//			MainSoundboard.botConfig.setActivity(Activity.streaming);
//			break;
		default:
			messageEv.getChannel().sendMessage("**Not** a vaild value!").submit();
			return false;
		}
		MainSoundboard.setActivity(args[1]);
		messageEv.getChannel().sendMessage("Activity changed to **"+MainSoundboard.botConfig.getActivity()+"**!").submit();
		return true;
	}

}
