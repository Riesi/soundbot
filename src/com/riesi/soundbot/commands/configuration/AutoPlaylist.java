package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotMode;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class AutoPlaylist extends Command {

	public AutoPlaylist() {
		this.name="autoplaylist";
		this.help="Sets the autoplaylist value.";
		this.arguments=" [true|false]";
		this.argumentCnt=2;
		this.modifiesCfg=true;
		this.mode=BotMode.music;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		switch (args[1]) {
		case "true":
			MainSoundboard.botConfig.getServerCfgs().get(messageEv.getGuild().getId()).setAutoplaylist(true);
			break;

		case "false":
			MainSoundboard.botConfig.getServerCfgs().get(messageEv.getGuild().getId()).setAutoplaylist(false);
			break;
		default:
			messageEv.getChannel().sendMessage("**Not** a vaild value!").submit();
			return false;
		}
		messageEv.getChannel().sendMessage("Autoplaylist changed to **"
				+MainSoundboard.botConfig.getServerCfgs()
				.get(messageEv.getGuild().getId()).isAutoplaylist()+"**!").submit();
		return true;
	}

}
