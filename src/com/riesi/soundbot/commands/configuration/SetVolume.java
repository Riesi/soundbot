package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.audio.RevampContainer;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class SetVolume extends Command {

	public SetVolume() {
		this.name="volume";
		this.help="Sets the volume of the bot.";
		this.arguments=" [0...100]";
		this.permissionLevel= BotPermission.moderator;
		this.modifiesCfg=true;
		this.argumentCnt=1;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		if(args.length>=2) {
			try {
				int vol=Integer.parseInt(args[1]);
				vol=vol>100?100:vol<0?0:vol;
				MainSoundboard.botConfig.getServerCfgs().get(messageEv.getGuild().getId()).setVolume(vol);
				if(RevampContainer.GET_INSTANCE().setVolume(messageEv.getGuild().getId(), vol)) {
					messageEv.getChannel().sendMessage("Volume set to **"+serverCfg.getVolume()+"%**.").submit();
				}else {
					messageEv.getChannel().sendMessage("Volume could **not** be set!").submit();
				}
				
				return true;
			}catch(NumberFormatException e) {
				messageEv.getChannel().sendMessage("Invalid Argument **not a number**!").submit();
			}
		}else {
			messageEv.getChannel().sendMessage("Current volume is **"+serverCfg.getVolume()+"%**.").submit();
		}
		
		return false;
	}

}
