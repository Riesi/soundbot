package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

import net.dv8tion.jda.api.entities.Message;

public class SetCooldown extends Command {

	public SetCooldown() {
		this.name = "cooldown";
		this.help = "Sets the cooldown of the bot.";
		this.arguments = " [0..." + Utilities.cooldownLimit + "]";
		this.permissionLevel = BotPermission.admin;
		this.modifiesCfg = true;
		this.argumentCnt = 1;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if (args.length >= 2) {
			try {
				long cd = Long.parseLong(args[1]);
				cd = cd < 0 ? 0 : cd > Utilities.cooldownLimit ? Utilities.cooldownLimit : cd;// limit cooldown
				serverCfg.setCooldown(cd);
				if (cd == 0) {
					messageEv.getChannel().sendMessage("Cooldown is now **disabled**!").submit();
				} else {
					messageEv.getChannel().sendMessage("Cooldown set to **" + serverCfg.getCooldown() + "s**.").submit();
				}
				return true;
			} catch (NumberFormatException e) {
				messageEv.getChannel().sendMessage("Invalid argument not a number").submit();
			}
		} else {
			messageEv.getChannel().sendMessage("Current cooldown is **" + serverCfg.getCooldown() + "s**.").submit();
		}

		return false;
	}

}
