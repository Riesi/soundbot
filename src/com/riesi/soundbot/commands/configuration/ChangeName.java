package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.exceptions.RateLimitedException;

public class ChangeName extends Command {

	public ChangeName() {
		this.name="changename";
		this.help="Changes the name of the bot.";
		this.arguments=" [NAME]";
		this.argumentCnt=2;
		this.permissionLevel=BotPermission.owner;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		
		try {
			StringBuffer bu = new StringBuffer();
			for(int i=1;i<args.length;i++) {//reconstruct spaces
				bu.append(args[i]+" ");
			}
			messageEv.getJDA().getSelfUser().getManager().setName(bu.toString()).complete(false);
			messageEv.getChannel().sendMessage("Name successfully changed!").submit();
			return true;
		} catch (RateLimitedException e) {
			messageEv.getChannel().sendMessage("Maximum 2 name changes per hour!").submit();
		} catch (Exception e) {
			e.printStackTrace();
			messageEv.getChannel().sendMessage("Halt and catch fire!").submit();
		}
		return false;
	}

}
