package com.riesi.soundbot.commands.configuration;

import java.io.File;

import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.apache.commons.io.FilenameUtils;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import com.riesi.soundbot.util.Utilities;

public class ChangePicture extends Command {

	public ChangePicture() {
		this.name="changepicture";
		this.help="Changes the picture of the bot to the picture in the attachment.";
		this.permissionLevel=BotPermission.owner;
		this.privateUse=true;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if(!messageEv.getAttachments().isEmpty()) {
			Message.Attachment att =messageEv.getAttachments().get(0);
			if(FilenameUtils.isExtension(att.getFileName(), Utilities.imageExtensions)) {
				File ico = new File(att.getFileName());
				att.downloadToFile(ico);
				
				try {
					messageEv.getJDA().getSelfUser().getManager().setAvatar(Icon.from(ico)).complete(true);
					ico.delete();
					messageEv.getChannel().sendMessage("Image successfully changed!").submit();
					return true;
				} catch (RateLimitedException e) {
					messageEv.getChannel().sendMessage("Maximum limit of changes per hour reached!").submit();
				} catch (Exception e) {
					e.printStackTrace();
					messageEv.getChannel().sendMessage("Halt and catch fire!").submit();
				}
			}else{
				messageEv.getChannel().sendMessage("Attached file is **no image** file!").submit();
			}	
		}else{
			messageEv.getChannel().sendMessage("**No** attachment found!").submit();
		}
		
		return false;
	}

}
