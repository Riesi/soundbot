package com.riesi.soundbot.commands.configuration;

import com.riesi.soundbot.MainSoundboard;
import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;


public class SetPrefix extends Command{
	public SetPrefix() {
		this.name="prefix";
		this.help="Sets the prefix to the given character.";
		this.arguments=" [PREFIX]";
		this.argumentCnt=2;
		this.permissionLevel=BotPermission.owner;
		this.modifiesCfg=true;
		this.privateUse=true;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		if(args[1].length()>1) {//restrict prefixes for now; maybe I get a better idea some time
			messageEv.getChannel().sendMessage("Prefix can only be a **single** character!").submit();
			return false;
		}else {
			MainSoundboard.botConfig.setPrefix(args[1]);
			messageEv.getChannel().sendMessage("Prefix changed to **"+MainSoundboard.botConfig.getPrefix()+"**!").submit();
			MainSoundboard.setActivity(null);
			return true;
		}
		
	}
}
