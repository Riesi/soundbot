package com.riesi.soundbot.commands.moderation;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;


public class MakeUser extends Command {

	public MakeUser() {
		this.name="makeuser";
		this.help="Makes the given user-id or role-id an user.";
		this.arguments=" [USER_ID|ROLE_ID]";
		this.permissionLevel=BotPermission.moderator;
		this.modifiesCfg=true;
		this.argumentCnt=2;
	}

	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		for(var guild: messageEv.getJDA().getGuilds()) {
			try {
				if(guild.getMemberById(args[1])!=null || guild.getRoleById(args[1]) != null) {
					if(serverCfg.getBlacklist().contains(args[1])) {
						messageEv.getChannel().sendMessage("You **can't** give permissions to a blacklisted **id**!").submit();
						return false;
					}
					serverCfg.getRoles().put(args[1], BotPermission.user);
					messageEv.getChannel().sendMessage("Given **id** is now an user!").submit();
					return true;
				}
			}catch(NumberFormatException e) {
				messageEv.getChannel().sendMessage("Given **id** is not valid!").submit();
				return false;
			}
		}
		messageEv.getChannel().sendMessage("No user with given **id** found!").submit();
		return false;
	}

}
