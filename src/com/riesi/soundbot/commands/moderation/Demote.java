package com.riesi.soundbot.commands.moderation;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;

public class Demote extends Command {

	public Demote() {
		this.name="demote";
		this.help="Demotes the given user-id or role-id.";
		this.arguments=" [USER_ID|ROLE_ID]";
		this.modifiesCfg=true;
		this.permissionLevel=BotPermission.moderator;
		this.argumentCnt=2;
	}
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		for(var guild: messageEv.getJDA().getGuilds()) {
			try {
				if(guild.getMemberById(args[1])!=null || guild.getRoleById(args[1]) != null) {
					BotPermission blperm = serverCfg.getRoles().get(args[1]);
					if((blperm == null)||blperm.getPermissionValue()<perm.getPermissionValue()) {
						serverCfg.getRoles().remove(args[1]);//remove given permissions!
						messageEv.getChannel().sendMessage("User / Role with given **id** is now demoted!").submit();
						return true;
					}
					messageEv.getChannel().sendMessage("You **can't** demote an user with the same or higher permission level!").submit();
					return false;
				}
			}catch(NumberFormatException e) {
				messageEv.getChannel().sendMessage("Given **id** is not valid!").submit();
				return false;
			}
		}
		messageEv.getChannel().sendMessage("No user with given **id** found!").submit();
		return false;
	}

}
