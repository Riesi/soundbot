package com.riesi.soundbot.commands.moderation;

import com.riesi.soundbot.commands.Command;
import com.riesi.soundbot.config.BotPermission;
import com.riesi.soundbot.config.ServerStruct;
import net.dv8tion.jda.api.entities.Message;


public class Pardon extends Command {

	public Pardon() {
		this.name="pardon";
		this.help="Removes given id from blacklist.";
		this.arguments=" [USER_ID]";
		this.modifiesCfg=true;
		this.permissionLevel=BotPermission.moderator;
		this.argumentCnt=2;
	}
	
	@Override
	public boolean executeCommand(Message messageEv, String[] args, BotPermission perm, ServerStruct serverCfg) {
		for(var guild: messageEv.getJDA().getGuilds()) {
			try {
				if(guild.getMemberById(args[1])!=null) {				
					serverCfg.getBlacklist().remove(args[1]);
					messageEv.getChannel().sendMessage("User with given **id** is now of the blacklist!").submit();
					return true;
				}
				
			}catch(NumberFormatException e) {
				messageEv.getChannel().sendMessage("Given **id** is not valid!").submit();
				return false;
			}
		}
		messageEv.getChannel().sendMessage("No user with given **id** found!").submit();
		return false;
	}

}
