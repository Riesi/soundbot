# The musicbot is broken... this will take some time to fix.
# WIP - SoundBot

This is a work in progress SoundBot for Discord, if you find any bugs or error please submit an issue!

A SoundBot for Discord with Music and Soundboard functionality. Created with [JDA](https://github.com/DV8FromTheWorld/JDA) and [lavaplayer](https://github.com/sedmelluq/lavaplayer). Inspired by the awesome musicbot from [jargosh](https://github.com/jagrosh/MusicBot) and the soundboardbot by [Darkside138](https://github.com/Darkside138/DiscordSoundboard).

## Releases
The latest release can be found [here](https://github.com/Riesi/SoundBot/releases/latest).

## Functionalities
* music playback via Youtube, Soundcloud and everything else [lavaplayer](https://github.com/sedmelluq/lavaplayer) supports
* playback of local files as soundboard
* playing of local playlists
* ...


## Getting started
Everything you need to run a bot.

### Java
The bot requires **java-11**, since there have been large changes to older versions.

### Configuration
When you have java-11 installed start the bot with `java -jar SoundBot.jar` once so it can create the config file, which you need to edit. You have to set the owner-id and token, everything else is optional. To acquire the owner-id right click on the specific user in discord and copy the id. For the token you need to create an application in the discord developer options.

### Create application
Create an application [here](https://discordapp.com/developers/applications). Go to the bot section and press **add bot**. Here you can set the name and picture of the bot (you can change it via commands afterwards). Reveal the bot-token and copy it into the config file.

## Usage

### Connect bot to a server
If you did everything correct and start the bot once again with `java -jar SoundBot.jar` he should boot up successful and spit out some logging. At the end of the output should be an invite link, which you can use to invite your bot to your server.

### Finish
Now the bot should be on your server and ready to use. By default the bot is configured as a musicbot, but you can change this via the changemode command!

Try the help command for informations about the other commands. 


### Side Notes
The audiofiles folder is used for the soundboard feature, so paste the files you want to use in there.
The playlists folder is dedicated for playlists, which are just a collection of links to the tracks you want to play. The bot can create autoplaylists for every server it's connected to

## Configuration file
You can edit the config file with the specific bot commands or manually. But be aware that you don't break the file syntax.
